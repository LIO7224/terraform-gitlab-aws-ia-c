variable "region" {
  description = "AWS region"
  type        = string
  default     = "us-east-1"
}

# comment acesskey and secret key its define in gitlab repo conf
#variable "AccessKey" {
#  description = "AWS Access Key ID"
#  type        = string
#  sensitive   = true
#}

#variable "SecretKey" {
#  description = "AWS Secret Access Key"
#  type        = string
#  sensitive   = true
#}